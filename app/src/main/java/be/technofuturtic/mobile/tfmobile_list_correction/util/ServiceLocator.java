package be.technofuturtic.mobile.tfmobile_list_correction.util;

import com.google.common.collect.Maps;

import java.util.Map;

public class ServiceLocator {
    //region singleton
    private static ServiceLocator instance = null;
    public static ServiceLocator getInstance() {
        if (instance == null) {
            instance = new ServiceLocator();
        }

        return instance;
    }
    //endregion

    private Map<Class<?>, Object> services;

    private ServiceLocator() {
        this.services = Maps.newHashMap();
    }

    public <T> void add(T obj) {
        this.services.put(obj.getClass(), obj);
    }
    public <T> T get(Class<T> tClass) {
        return (T) this.services.get(tClass);
    }
    public <T> void remove(Class<T> tClass) {
        this.services.remove(tClass);
    }
}
