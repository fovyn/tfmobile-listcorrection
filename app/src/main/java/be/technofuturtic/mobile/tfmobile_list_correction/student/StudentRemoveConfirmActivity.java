package be.technofuturtic.mobile.tfmobile_list_correction.student;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import be.technofuturtic.mobile.tfmobile_list_correction.R;

public class StudentRemoveConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_remove_confirm);
    }
}