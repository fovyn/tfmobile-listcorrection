package be.technofuturtic.mobile.tfmobile_list_correction.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.lang.reflect.Field;
import java.util.List;

public abstract class BaseDAO<TKey, TValue> implements DAO<TKey, TValue> {
    protected Context context;
    protected DbHelper dbHelper;

    public BaseDAO(Context context) {
        this.context = context;
        this.dbHelper = new DbHelper(context);
    }

    public final SQLiteDatabase openReadable() {
        return this.dbHelper.getReadableDatabase();
    }
    public final SQLiteDatabase openWritable() {
        return this.dbHelper.getWritableDatabase();
    }

    protected abstract boolean isIntField(String field);
    protected abstract boolean isLongField(String field);
    protected abstract boolean isStringField(String field);
}
