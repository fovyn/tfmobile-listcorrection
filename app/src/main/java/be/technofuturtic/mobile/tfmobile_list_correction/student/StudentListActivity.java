package be.technofuturtic.mobile.tfmobile_list_correction.student;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import be.technofuturtic.mobile.tfmobile_list_correction.R;
import be.technofuturtic.mobile.tfmobile_list_correction.section.dao.SectionDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.section.model.Section;
import be.technofuturtic.mobile.tfmobile_list_correction.student.adapter.StudentAdapter;
import be.technofuturtic.mobile.tfmobile_list_correction.student.dao.StudentDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;
import be.technofuturtic.mobile.tfmobile_list_correction.util.Constant;
import be.technofuturtic.mobile.tfmobile_list_correction.util.ServiceLocator;

public class StudentListActivity extends AppCompatActivity {


    private ListView lvStudent;
    private StudentAdapter adapter;
    private List<Student> students;
    private Button btnCreate;

    private StudentDAO studentDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);

        this.lvStudent = findViewById(R.id.activity_student_lvStudent);
        this.btnCreate = findViewById(R.id.activity_student_btnCreate);

        this.studentDAO = new StudentDAO(StudentListActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        this.btnCreate.setOnClickListener(this::goToCreate);

        this.students = this.studentDAO.readAll();
        List<Section> sections = ServiceLocator.getInstance().get(SectionDAO.class).readAll();

        for(Section section: sections) {
            Log.d(this.getClass().getSimpleName(), section.toString());
            Log.d(this.getClass().getSimpleName(), section.getDirector().toString());
        }
        this.adapter = new StudentAdapter(StudentListActivity.this, this.students);
        this.adapter.setEditAction(this::goToEdit);
        this.lvStudent.setAdapter(adapter);
    }

    private void goToCreate(View view) {
        Intent intentToCreate = new Intent(StudentListActivity.this, StudentCreateActivity.class);

        startActivityForResult(intentToCreate, Constant.REQUEST_CODE.CODE_STUDENT_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Constant.REQUEST_CODE.CODE_STUDENT_EDIT:
                editResultAction(resultCode, data);
                break;
            case Constant.REQUEST_CODE.CODE_STUDENT_CREATE:
                createResultAction(resultCode, data);
                break;
        }
    }

    private void createResultAction(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Student student = data.getParcelableExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT);
            this.studentDAO.insert(student);
            this.adapter.add(student);
        }
    }

    private void editResultAction(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Student student = data.getParcelableExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT);

            Optional<Student> toEdit = this.students.stream().parallel()
                    .filter(s -> s.getNoma().equals(student.getNoma())).findFirst();

            toEdit.ifPresent(s -> this.adapter.remove(s));
            this.adapter.add(student);
        }
    }

    private void goToEdit(Student student) {
        Intent editIntent = new Intent(StudentListActivity.this, StudentEditActivity.class);

        editIntent.putExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT, student);

        startActivityForResult(editIntent, Constant.REQUEST_CODE.CODE_STUDENT_EDIT);
    }
}