package be.technofuturtic.mobile.tfmobile_list_correction.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import be.technofuturtic.mobile.tfmobile_list_correction.professor.dao.ProfessorDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.section.dao.SectionDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.student.dao.StudentDAO;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "TFMobile_List_Correction";
    public static final int DB_VERSION = 1;


    public DbHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(StudentDAO.SQL_CREATE);
        db.execSQL(ProfessorDAO.SQL_CREATE);
        db.execSQL(SectionDAO.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(StudentDAO.SQL_UPGRADE);
        db.execSQL(ProfessorDAO.SQL_UPGRADE);
    }
}
