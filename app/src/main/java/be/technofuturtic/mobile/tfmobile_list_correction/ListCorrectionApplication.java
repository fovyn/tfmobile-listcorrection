package be.technofuturtic.mobile.tfmobile_list_correction;

import android.app.Application;

import be.technofuturtic.mobile.tfmobile_list_correction.professor.dao.ProfessorDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.section.dao.SectionDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.student.dao.StudentDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.util.ServiceLocator;

public class ListCorrectionApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ServiceLocator locator = ServiceLocator.getInstance();

        locator.add(new StudentDAO(getApplicationContext()));
        locator.add(new ProfessorDAO(getApplicationContext()));
        locator.add(new SectionDAO(getApplicationContext()));
    }
}
