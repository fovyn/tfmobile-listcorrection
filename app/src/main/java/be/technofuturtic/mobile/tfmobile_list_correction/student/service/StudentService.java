package be.technofuturtic.mobile.tfmobile_list_correction.student.service;

import com.google.common.collect.Comparators;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;

public class StudentService {
    private List<Student> students;
    private static int nbStudent = 0;

    public StudentService() {
        this.students = new ArrayList<>();
    }

    public Student create(Student student) {
        student.setNoma("Inra"+ nbStudent);

        this.students.add(student);

        return student;
    }

    public void edit(String noma, Student obj) {
        Optional<Student> optionalToEdit = this.students.parallelStream()
                .filter(s -> s.getNoma().equals(noma)).findFirst();

        optionalToEdit.ifPresent(s -> {
            s.setFirstName(obj.getFirstName());
            s.setLastName(obj.getLastName());
        });
    }
}
