package be.technofuturtic.mobile.tfmobile_list_correction.actions;

@FunctionalInterface
public interface Action<T> {
    void execute(T object);
}
