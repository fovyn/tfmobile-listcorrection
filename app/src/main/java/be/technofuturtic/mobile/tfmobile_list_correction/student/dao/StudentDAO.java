package be.technofuturtic.mobile.tfmobile_list_correction.student.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.collect.Lists;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import be.technofuturtic.mobile.tfmobile_list_correction.db.BaseDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.db.DbHelper;
import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;

public class StudentDAO extends BaseDAO<Long, Student> {
    public static final String TABLE_NAME = "student";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_NOMA = "student_noma";
    public static final String FIELD_LAST_NAME = "student_lastName";
    public static final String FIELD_FIRST_NAME = "student_firstName";
    public static final String FIELD_EMAIL = "student_email";

    public static final String SQL_CREATE = String.format("CREATE TABLE %s (%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, %s VARCHAR(50) NOT NULL, %s VARCHAR(50) NOT NULL, %s VARCHAR(50) NOT NULL, %s VARCHAR(50) NOT NULL, CONSTRAINT UK_Student_Noma UNIQUE (%s))", TABLE_NAME, FIELD_ID, FIELD_NOMA, FIELD_LAST_NAME, FIELD_FIRST_NAME, FIELD_EMAIL, FIELD_NOMA);

    public static final String SQL_UPGRADE = "DROP TABLE IF EXISTS "+ TABLE_NAME;

    private DbHelper dbHelper;
    private Context context;

    public StudentDAO(@NonNull Context context) {
        super(context);
    }

    @Override
    public Student toEntity(Cursor cursor) {
        long id = cursor.getLong(cursor.getColumnIndex(FIELD_ID));
        String noma = cursor.getString(cursor.getColumnIndex(FIELD_NOMA));
        String lastName = cursor.getString(cursor.getColumnIndex(FIELD_LAST_NAME));
        String firstName = cursor.getString(cursor.getColumnIndex(FIELD_FIRST_NAME));
        String email = cursor.getString(cursor.getColumnIndex(FIELD_EMAIL));

        return new Student(id, noma, lastName, firstName, email);
    }

    public Student toEntity(Cursor cursor, String... fields) {
        Student s = new Student();

        for(String field: fields) {
            int index = cursor.getColumnIndex(field);
            if (isLongField(field)) {
                long fieldValue = cursor.getLong(index);
            } else if (isStringField(field)) {
                String fieldValue = cursor.getString(index);
            }
        }

        return s;
    }

    @Override
    protected boolean isIntField(String field) {
        List<String> stringFields = Lists.newArrayList();
        return stringFields.stream().anyMatch(f -> f.equals(field));
    }

    @Override
    protected boolean isLongField(String field) {
        List<String> stringFields = Lists.newArrayList(
                FIELD_ID);
        return stringFields.stream().anyMatch(f -> f.equals(field));
    }

    @Override
    protected boolean isStringField(String field) {
        List<String> stringFields = Lists.newArrayList(
                FIELD_NOMA,
                FIELD_FIRST_NAME,
                FIELD_LAST_NAME,
                FIELD_EMAIL);
        return stringFields.stream().anyMatch(f -> f.equals(field));
    }

    @Override
    public List<Student> readAll() {
        List<Student> students = Lists.newArrayList();

        try (SQLiteDatabase db = this.openReadable()) {
            Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            while(cursor.moveToNext()) {
                students.add(toEntity(cursor));
            }
        } catch (SQLException e) {
            Log.e("StudentDAO.Read", Objects.requireNonNull(e.getMessage()));
        }

        return students;
    }

    @Override
    public Student readByKey(Long aLong) {
        try (SQLiteDatabase db = this.openReadable()) {
            Cursor cursor = db.query(
                    TABLE_NAME,
                    null,
                    String.format("%s = %d", FIELD_ID, aLong),
                    null,
                    null,
                    null,
                    null
                    );

            return toEntity(cursor);
        } catch (SQLException e) {
            Log.e("StudentDAO.Read", Objects.requireNonNull(e.getMessage()));
            return null;
        }
    }

    public Student insert(@Nullable() Student student) {
        if (student == null) return null;

        ContentValues cv = new ContentValues();

        try (SQLiteDatabase db = this.openWritable()) {
            cv.put(FIELD_NOMA, student.createNoma());
            cv.put(FIELD_LAST_NAME, student.getLastName());
            cv.put(FIELD_FIRST_NAME, student.getFirstName());
            cv.put(FIELD_EMAIL, student.getEmail());
            student.setId(db.insert(TABLE_NAME, null, cv));
        } catch (SQLException e) {
            Log.e("StudentDAO.Insert", e.getMessage());
        }
        return student;
    }

    @Override
    public Student update(Long aLong, Student obj) {
        ContentValues cv = new ContentValues();
        cv.put(FIELD_EMAIL, obj.getEmail());
        cv.put(FIELD_NOMA, obj.getNoma());
        cv.put(FIELD_FIRST_NAME, obj.getFirstName());
        cv.put(FIELD_LAST_NAME, obj.getLastName());

        try (SQLiteDatabase db = this.openWritable()) {
            db.update(TABLE_NAME, cv, FIELD_ID+ " = "+ aLong, null);
        }
        return obj;
    }

    @Override
    public void remove(Long aLong) {
        try (SQLiteDatabase db = this.openWritable()) {
            db.delete(TABLE_NAME, FIELD_ID+ " = "+ aLong, null);
        } catch (Exception e){

        }
    }

}
