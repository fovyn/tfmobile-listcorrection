package be.technofuturtic.mobile.tfmobile_list_correction.db;

import android.database.Cursor;

import java.util.List;

public interface DAO<TKey, TValue> {
    TValue toEntity(Cursor cursor, String... fields);
    TValue toEntity(Cursor cursor);
    List<TValue> readAll();
    TValue readByKey(TKey key);
    TValue insert(TValue obj);
    TValue update(TKey key, TValue obj);
    void remove(TKey key);
}
