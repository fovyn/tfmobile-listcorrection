package be.technofuturtic.mobile.tfmobile_list_correction.professor.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;

import java.util.List;

import be.technofuturtic.mobile.tfmobile_list_correction.db.BaseDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.professor.model.Professor;

public class ProfessorDAO extends BaseDAO<Long, Professor> {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_NOMA = "professor_noma";
    public static final String FIELD_LAST_NAME = "professor_lastName";
    public static final String FIELD_FIRST_NAME = "professor_firstName";

    public static final String TABLE_NAME = "professor";
    public static final String SQL_CREATE = "CREATE TABLE "+ TABLE_NAME+ "("
            + FIELD_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FIELD_NOMA+ " VARCHAR(50) NOT NULL,"
            + FIELD_LAST_NAME+ " VARCHAR(50) NOT NULL,"
            + FIELD_FIRST_NAME+ " VARCHAR(50) NOT NULL)";
    public static final String SQL_UPGRADE = "DROP TABLE professor";

    public ProfessorDAO(Context context) {
        super(context);
    }

    @Override
    protected boolean isIntField(String field) {
        return false;
    }

    @Override
    protected boolean isLongField(String field) {
        return false;
    }

    @Override
    protected boolean isStringField(String field) {
        return false;
    }

    @Override
    public Professor toEntity(Cursor cursor, String... fields) {
        return null;
    }

    @Override
    public Professor toEntity(Cursor cursor) {
        Professor professor = new Professor();

        professor.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
        professor.setNoma(cursor.getString(cursor.getColumnIndex(FIELD_NOMA)));
        professor.setLastName(cursor.getString(cursor.getColumnIndex(FIELD_LAST_NAME)));
        professor.setFirstName(cursor.getString(cursor.getColumnIndex(FIELD_FIRST_NAME)));

        return professor;
    }

    @Override
    public List<Professor> readAll() {
        List<Professor> professors = Lists.newArrayList();
        try (SQLiteDatabase db = this.openReadable()) {
            Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                professors.add(toEntity(cursor));
            }
        }
        return professors;
    }

    @Override
    public Professor readByKey(Long aLong) {
        try (SQLiteDatabase db = this.openReadable()) {
            Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            cursor.moveToFirst();

            return toEntity(cursor);
        } catch (Exception e) {

        }
        return null;
    }

    @Override
    public Professor insert(Professor obj) {
        return null;
    }

    @Override
    public Professor update(Long aLong, Professor obj) {
        return null;
    }

    @Override
    public void remove(Long aLong) {

    }
}
