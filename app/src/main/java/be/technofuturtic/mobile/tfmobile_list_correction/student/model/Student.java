package be.technofuturtic.mobile.tfmobile_list_correction.student.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"noma", "lastName"})
@ToString(of = {"noma", "lastName", "firstName", "email"})
@NoArgsConstructor
@AllArgsConstructor
public class Student implements Parcelable{
    private long id;
    private String noma;
    private String lastName;
    private String firstName;
    private String email;

    public String createNoma() {
        StringBuilder builder = new StringBuilder();

        builder.append(LocalDate.now().getYear()).append("-");
        builder.append(lastName.substring(0,3)).append(firstName.substring(0,3));

        return builder.toString();
    }

    protected Student(Parcel in) {
        id = in.readLong();
        noma = in.readString();
        lastName = in.readString();
        firstName = in.readString();
        email = in.readString();
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(noma);
        dest.writeString(lastName);
        dest.writeString(firstName);
        dest.writeString(email);
    }
}
