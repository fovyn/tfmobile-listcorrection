package be.technofuturtic.mobile.tfmobile_list_correction.util;

public class Constant {
    public static class INTENT_EXTRA {
        public static final String EXTRA_STUDENT = "student";
    }

    public static class REQUEST_CODE {
        public static final int CODE_STUDENT_EDIT = 101;
        public static final int CODE_STUDENT_CREATE = 102;
    }
}
