package be.technofuturtic.mobile.tfmobile_list_correction.student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import be.technofuturtic.mobile.tfmobile_list_correction.R;
import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;
import be.technofuturtic.mobile.tfmobile_list_correction.util.Constant;

public class StudentCreateActivity extends AppCompatActivity {
    private EditText etLastName;
    private EditText etFirstName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_create);

        etLastName = findViewById(R.id.activity_student_create_etLastName);
        etFirstName = findViewById(R.id.activity_student_create_etFirstName);

        Button btnCreate = findViewById(R.id.activity_student_create_btnCreate);

        btnCreate.setOnClickListener(this::createAction);
    }

    private void createAction(View view) {
        Student student = new Student();
        student.setLastName(etLastName.getText().toString());
        student.setFirstName(etFirstName.getText().toString());
        student.setEmail(String.format("%s.%s@inra.be", student.getLastName(), student.getFirstName()));

        Intent intent = new Intent();
        intent.putExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT, student);

        setResult(RESULT_OK, intent);
        finish();
    }
}