package be.technofuturtic.mobile.tfmobile_list_correction.section.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.google.common.collect.Lists;

import java.util.List;

import be.technofuturtic.mobile.tfmobile_list_correction.db.BaseDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.professor.dao.ProfessorDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.professor.model.Professor;
import be.technofuturtic.mobile.tfmobile_list_correction.section.model.Section;
import be.technofuturtic.mobile.tfmobile_list_correction.util.ServiceLocator;

public class SectionDAO extends BaseDAO<Long, Section> {
    public static final String FIELD_ID = "_id";
    public static final String FIELD_LABEL = "section_label";
    public static final String FIELD_DIRECTOR_ID = "director_id";

    public static final String TABLE_NAME = "section";
    public static final String SQL_CREATE = "CREATE TABLE "+ TABLE_NAME+ "("
            + FIELD_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FIELD_LABEL+ " VARCHAR(50) NOT NULL,"
            + FIELD_DIRECTOR_ID+ " INTEGER NOT NULL,"
            + "CONSTRAINT FK_Section_Professor FOREIGN KEY ("+ FIELD_DIRECTOR_ID+ ") REFERENCES Professor(_id))";

    public SectionDAO(Context context) {
        super(context);
    }

    @Override
    protected boolean isIntField(String field) {
        return false;
    }

    @Override
    protected boolean isLongField(String field) {
        return false;
    }

    @Override
    protected boolean isStringField(String field) {
        return false;
    }

    @Override
    public Section toEntity(Cursor cursor, String... fields) {
        return null;
    }

    @Override
    public Section toEntity(Cursor cursor) {
        Section section = new Section();

        section.setId(cursor.getLong(cursor.getColumnIndex(FIELD_ID)));
        section.setLabel(cursor.getString(cursor.getColumnIndex(FIELD_LABEL)));
        section.setDirectorId(cursor.getLong(cursor.getColumnIndex(FIELD_DIRECTOR_ID)));

        return section;
    }

    @Override
    public List<Section> readAll() {
        List<Section> professors = Lists.newArrayList();
        try (SQLiteDatabase db = this.openReadable()) {
            Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                professors.add(toEntity(cursor));
            }
        }
        return professors;
    }

    @Override
    public Section readByKey(Long aLong) {
        return null;
    }

    @Override
    public Section insert(Section obj) {
        return null;
    }

    @Override
    public Section update(Long aLong, Section obj) {
        return null;
    }

    @Override
    public void remove(Long aLong) {

    }
}

