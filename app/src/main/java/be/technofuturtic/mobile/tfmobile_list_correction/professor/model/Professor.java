package be.technofuturtic.mobile.tfmobile_list_correction.professor.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"noma", "lastName"})
@ToString(of = {"noma", "lastName", "firstName"})
@NoArgsConstructor
@AllArgsConstructor
public class Professor {
    private long id;
    private String noma;
    private String lastName;
    private String firstName;
}
