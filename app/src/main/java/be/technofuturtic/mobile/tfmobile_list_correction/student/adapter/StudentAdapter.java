package be.technofuturtic.mobile.tfmobile_list_correction.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import be.technofuturtic.mobile.tfmobile_list_correction.R;
import be.technofuturtic.mobile.tfmobile_list_correction.actions.Action;
import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;

public class StudentAdapter extends ArrayAdapter<Student> {
    private Action<Student> editAction;
    private Action<Student> removeAction;

    public StudentAdapter(@NonNull Context context, @NonNull List<Student> objects) {
        super(context, R.layout.adapter_student_list_item, objects);
    }

    public void setEditAction(Action<Student> editAction) {
        this.editAction = editAction;
    }

    public void setRemoveAction(Action<Student> removeAction) {
        this.removeAction = removeAction;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final Student student = this.getItem(position);
        if (student == null) return convertView;
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_student_list_item, parent, false);
        }

        TextView tvLastName = convertView.findViewById(R.id.adapter_student_list_item_lastName);
        TextView tvFirstName = convertView.findViewById(R.id.adapter_student_list_item_firstName);
        Button btnEdit = convertView.findViewById(R.id.adapter_student_btnEdit);
        Button btnRemove = convertView.findViewById(R.id.adapter_student_btnRemove);

        tvLastName.setText(student.getLastName());
        tvFirstName.setText(student.getFirstName());

        if (this.editAction != null) {
            btnEdit.setOnClickListener(v -> this.editAction.execute(student));
        }

        if (this.removeAction != null) {
            btnRemove.setOnClickListener(v -> this.removeAction.execute(student));
        } else {
            btnRemove.setOnClickListener(v -> this.remove(student));
        }

        return convertView;
    }
}
