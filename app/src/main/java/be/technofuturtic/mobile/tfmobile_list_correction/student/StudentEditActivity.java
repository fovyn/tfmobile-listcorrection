package be.technofuturtic.mobile.tfmobile_list_correction.student;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import be.technofuturtic.mobile.tfmobile_list_correction.R;
import be.technofuturtic.mobile.tfmobile_list_correction.student.model.Student;
import be.technofuturtic.mobile.tfmobile_list_correction.util.Constant;

public class StudentEditActivity extends AppCompatActivity {
    private EditText etLastName;
    private EditText etFirstName;

    private Button btnEdit;
    private Button btnClear;

    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_edit);

        this.etLastName = findViewById(R.id.activity_student_edit_etLastName);
        this.etFirstName = findViewById(R.id.activity_student_edit_etFirstName);

        this.btnEdit = findViewById(R.id.activity_student_edit_btnEdit);
        this.btnClear = findViewById(R.id.activity_student_edit_btnClear);

        this.btnEdit.setOnClickListener(this::editAction);
        this.btnClear.setOnClickListener(this::clearAction);
    }


    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            this.student = intent.getParcelableExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT);
            this.etFirstName.setText(this.student.getFirstName());
            this.etLastName.setText(this.student.getLastName());
        }
    }

    public void editAction(View v) {
        this.student.setFirstName(this.etFirstName.getText().toString());
        this.student.setLastName(this.etLastName.getText().toString());

        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constant.INTENT_EXTRA.EXTRA_STUDENT, this.student);

        setResult(RESULT_OK, resultIntent);
        finish();
    }
    private void clearAction(View v) {
        this.etFirstName.setText("");
        this.etLastName.setText("");
    }
}