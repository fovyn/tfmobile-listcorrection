package be.technofuturtic.mobile.tfmobile_list_correction.section.model;

import be.technofuturtic.mobile.tfmobile_list_correction.professor.dao.ProfessorDAO;
import be.technofuturtic.mobile.tfmobile_list_correction.professor.model.Professor;
import be.technofuturtic.mobile.tfmobile_list_correction.util.ServiceLocator;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode(of = {"label"})
@ToString(of = {"id", "label", "directorId", "director"},doNotUseGetters = true)
@NoArgsConstructor
@AllArgsConstructor
public class Section {
    private long id;
    private String label;
    private long directorId;
    private Professor director;

    /**
     * Méthode de chargement en LazyLoading du directeur
     * @return Professor
     */
    public Professor getDirector() {
        if (director == null) {
            this.director = ServiceLocator.getInstance()
                    .get(ProfessorDAO.class)
                    .readByKey(this.directorId);
        }

        return this.director;
    }
}
